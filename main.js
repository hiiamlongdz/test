var month = document.getElementById("month");
var day = document.getElementById("day");
var year = document.getElementById("year");
var dayOfMonth = 31;

for (let i = 1; i <= 12; i++) {
  var monthElement = document.createElement("option");
  monthElement.value = i;
  monthElement.text = i;

  month.add(monthElement);
}

var date = new Date();
for (let i = 1945; i <= date.getFullYear(); i++) {
  var yearElement = document.createElement("option");
  yearElement.value = i;
  yearElement.text = i;

  year.add(yearElement);
}

month.addEventListener("change", changeDay);
year.addEventListener("change", changeDay);

function changeDay() {
  switch (month.value) {
    case "1":
    case "3":
    case "5":
    case "7":
    case "8":
    case "10":
    case "12":
      dayOfMonth = 31;
      break;

    case "4":
    case "6":
    case "9":
    case "11":
      dayOfMonth = 30;
      break;

    case "2":
      if (year.value % 4 == 0) {
        dayOfMonth = 29;
      } else {
        dayOfMonth = 28;
      }
      break;

    default:
      break;
  }

  var options = document.querySelectorAll("#day option");
  options.forEach((o) => o.remove());

  for (let i = 1; i <= dayOfMonth; i++) {
    var dayElement = document.createElement("option");
    dayElement.value = i;
    dayElement.text = i;

    day.add(dayElement);
  }
}

for (let i = 1; i <= dayOfMonth; i++) {
  var dayElement = document.createElement("option");
  dayElement.value = i;
  dayElement.text = i;

  day.add(dayElement);
}
